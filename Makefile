# MACRO_FORMAT
# latex | xelatex
MACRO_FORMAT = xelatex

# Indicate which generation procedure to use. One in:
# pdf | dvi | ps | pspdf
LATEX_GENERATION_PROCEDURE = pdf

#HAS_MULTIPLE_BIB = yes
# For multbib
#MULTIPLE_BIB_FILES = default rec
# For bibunits
#MULTIPLE_BIB_FILES = bu*

# Shell command used to launch MakeIndex
# makeindex | xindy
MAKEINDEX_CMD = makeindex

# Style file for MakeIndex
MAKEINDEX_STYLEFILE = index.ist

#TEX4HT_FINAL_POST_CMD = scripts/html-fix

# Image directory
AUTO_GENERATE_IMAGE_DIRECTORY = image

# BibTeX command
#BIBTEX_CMD = bibtex

# Post LaTeX command
#POST_LATEX_CMD = scripts/post_latex_cmd

# Post BibTeX command
#POST_BIBTEX_CMD = scripts/post_bibtex_cmd

# Additional temporary files
TMPFILES_LOCAL = split-track.tex split-paper.tex
TMPDIRS_LOCAL = split-track.d split-paper.d

include mkfiles/main.mk

split-track: default.pdf
	./scripts/split-track

split-paper: default.pdf
	./scripts/split-paper
