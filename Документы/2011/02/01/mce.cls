\ProvidesClass{mce}[2009/10/05 v0.2.5]
\def\class@name{mce}%
\GenericInfo{}{\space
 Copyright (c) 2004 ITP TU Berlin.^^J
 Copyright (c) 2007-2009 MSU Moscow.^^J
 Licensed under the LPPL:^^Jhttp://www.ctan.org/tex-archive/macros/latex/base/lppl.txt^^J
 \@gobble
}%
\DeclareOption{eng}{
\gdef\@langeng{true}
      \PackageWarning{mce}{Debug: `\CurrentOption'}%
}
\gdef\@langeng{}
\ProcessOptions\relax
\LoadClass[12pt,a4paper]{article}
\RequirePackage{a4}
%\usepackage{pscyr}
\usepackage[math]{pscyr}
\usepackage[T2A]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage[english,russian]{babel}
\RequirePackage[fleqn]{amsmath}
\RequirePackage{amstext}
\RequirePackage{amsfonts}
\RequirePackage{ifthen}
\renewcommand{\rmdefault}{ftm}
\PrerenderUnicode{АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя–}
\setlength\textheight{230mm}
\setlength\textwidth{160mm}
\setlength\topmargin{-1mm}
\setlength\oddsidemargin{0mm}
\setlength\evensidemargin{0mm}

\sloppy

\AtBeginDocument{
  \pagestyle{empty}
  \bibliographystyle{alpha}
}

\gdef\@contacts{}
\gdef\@author{}
\def\author[#1]#2{
	\ifx\@author\@empty\else\g@addto@macro\@author{, }\fi
	\g@addto@macro\@author{\mbox{#2\ifx\\#1\\\else\@textsuperscript{\textbf{#1}}\fi}}
}
\def\contacts[#1]#2{
	\ifx\@contacts\@empty\else\g@addto@macro\@contacts{\par}\fi
	\g@addto@macro\@contacts{\ifx\\#1\\\else\@textsuperscript{\textbf{#1}}\fi#2}
}
\def\langeng#1{\gdef\@langeng{#1}}

\def\maketitle{
	\begin{center}
  	{\bf\MakeUppercase{\@title} \par}
	  \vskip 1em                         % Vertical space after title.
  	{\textbf{\@author}}
  	\vskip 1em                         % Vertical space after title.
  	{\@contacts}
  	\vskip 1.5em                         % Vertical space after title.
	\end{center} \par
	\let\maketitle\relax
	\def\documentclass##1{}
}

\renewenvironment{thebibliography}[1]
{
  \vskip 1em
	\ifx\@langeng\@empty\textbf{Литература.}\else\textbf{References.}\fi
  %\list{\@biblabel{\@arabic\c@enumiv}}%
  \list{\@arabic\c@enumiv.}%
  {
    \samepage
    \itemsep=0ex\topsep=0ex\partopsep=0ex\parskip=0ex\parsep=0ex
    \settowidth\labelwidth{\@biblabel{#1}}%
    \leftmargin\labelwidth
    \advance\leftmargin\labelsep
    \@openbib@code
    \usecounter{enumiv}%
    \let\p@enumiv\@empty
    \renewcommand\theenumiv{\@arabic\c@enumiv}}%
  \sloppy
  \clubpenalty4000
  \@clubpenalty \clubpenalty
  \widowpenalty4000%
  \sfcode`\.\@m}
{\def\@noitemerr
  {\@latex@warning{Empty `thebibliography' environment}}%
  \endlist}
